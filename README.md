# README #

Collection of CMake based project templates for Ogre.

### Currently contains: ###

* my_clean_ogre_cmake_project - framework (Ogre v1.9) from the Ogre Wiki Tutorials, including dist folder
* my_ogre+cegui_cmake_project - Ogre 1.9 + CEGUI 0.8.4 template based off Ogre Wiki "Tutorial 7"

### Notes: ###

* Uses CMake system to generate project files
* Currently tested on Windows 7 + Visual Studio 2015
* Some filenames are hard coded in the CMakeLists.txt file (eg. path to Boost lib .dll)
* My notes at the top of BaseApplication.h in my_ogre+cegui_cmake_project need updating
* CEGUI resource tree is not included in the repo